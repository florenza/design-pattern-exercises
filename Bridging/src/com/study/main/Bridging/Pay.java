package com.study.main.Bridging;

import java.math.BigDecimal;

public abstract class Pay {
    IPayMode payMode;

    public Pay(IPayMode payMode) {
        this.payMode = payMode;
    }

    abstract String transfer(String uid, BigDecimal amount);
}

package com.study.main.Bridging;

import java.math.BigDecimal;

public class BridginMain {
    public static void main(String[] args) {
        IPayMode faceTypeMethodPay = new FaceTypeMethodPay();
        WXPay wxPay = new WXPay(faceTypeMethodPay);
        wxPay.transfer("1111111-2222-3333", BigDecimal.valueOf(200));
        IPayMode passwordTypeMethodPay = new PasswordTypeMethodPay();
        ZFBPay zfbPay = new ZFBPay(passwordTypeMethodPay);
        zfbPay.transfer("222-33-239-2323", BigDecimal.valueOf(30000));
    }
}

package com.study.main.Bridging;

import java.math.BigDecimal;

public class WXPay extends Pay{
    public WXPay(IPayMode payMode) {
        super(payMode);
    }

    @Override
    String transfer(String uid, BigDecimal amount) {
        boolean security = payMode.security(uid);
        if(security){
            System.out.println("恭喜您,您用ZFB成功支付了"+amount+"元");
        }
        return uid;

    }
}

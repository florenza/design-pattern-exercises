package com.study.main.Adapter;

public class ChangeAdapter implements Usb{
    AndroidTypeC androidTypeC;
    public ChangeAdapter() {
        this.androidTypeC = new AndroidTypeC();
    }

    @Override
    public String useUsbMethod() {
        return androidTypeC.myUsbMethods();
    }
}

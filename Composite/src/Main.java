import com.study.composite.safe.CommonSafeCourse;
import com.study.composite.safe.LevelSafeCourse;
import com.study.composite.transprant.CommonCourse;
import com.study.composite.transprant.LevelCourse;

public class Main {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello world!");
        CommonCourse chinese = new CommonCourse("语文", "120");
        CommonCourse English = new CommonCourse("英语", "100");
        CommonCourse Math = new CommonCourse("数学", "140");
        LevelCourse levelCourse = new LevelCourse("公共课");
        levelCourse.addChild(chinese);
        levelCourse.addChild(English);
        levelCourse.addChild(Math);
        levelCourse.info();
        System.out.println("------------安全模式---------------------");
        CommonSafeCourse history = new CommonSafeCourse("历史","120");
        CommonSafeCourse politics = new CommonSafeCourse("历史","120");
        CommonSafeCourse geography = new CommonSafeCourse("历史","120");
        LevelSafeCourse levelSafeCourse = new LevelSafeCourse("文综", "360");
        levelSafeCourse.addChild(history);
        levelSafeCourse.addChild(politics);
        levelSafeCourse.addChild(geography);
        levelSafeCourse.info();

    }
}
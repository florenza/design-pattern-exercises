package com.study.composite.transprant;

import java.util.ArrayList;
import java.util.List;

public class LevelCourse extends GKAbstractCourse {
    private List<GKAbstractCourse> levelCourseList =new ArrayList<>();

    private String name;

    public LevelCourse(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void addChild(GKAbstractCourse gkAbstractCourse) {
       levelCourseList.add(gkAbstractCourse);
    }

    @Override
    public void info() throws Exception {
        for (GKAbstractCourse gkAbstractCourse : levelCourseList) {
            gkAbstractCourse.info();
        }
    }
}

package com.study.composite.transprant;

public class CommonCourse extends GKAbstractCourse {
    private String name;
    private String score;
    public CommonCourse(String name,String score){
        this.name = name;
        this.score = score;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getScore() {
        return score;
    }

    @Override
    public void info() throws Exception {
        System.out.println("科目:" + this.getName());
        System.out.println("分数:" + this.getScore());
    }
}

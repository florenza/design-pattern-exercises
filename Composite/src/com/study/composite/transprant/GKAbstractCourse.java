package com.study.composite.transprant;

public abstract class GKAbstractCourse {
    public void addChild(GKAbstractCourse gkAbstractCourse){
        System.out.println("不支持添加操作");
    }
    public String getName() throws Exception{
        throw new Exception("不支持获取名称");
    }
    public void info() throws Exception{
        throw new Exception("不支持查询");
    }

}

package com.study.composite.safe;

public abstract class GKAbstractSafeCourse {
   String name;
   String score;

    public GKAbstractSafeCourse(String name, String score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public String getScore() {
        return score;
    }

    public abstract void info();
}

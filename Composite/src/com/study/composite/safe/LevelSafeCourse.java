package com.study.composite.safe;

import java.util.ArrayList;
import java.util.List;

public class LevelSafeCourse extends GKAbstractSafeCourse {
    private List<GKAbstractSafeCourse> levelCourseList =new ArrayList<>();

    public LevelSafeCourse(String name, String score) {
        super(name, score);
    }
    public void addChild(GKAbstractSafeCourse gkAbstractSafeCourse){
        this.levelCourseList.add(gkAbstractSafeCourse);
    }
    @Override
    public void info() {
         System.out.println("科目:"+ this.getName() + "总分:" + this.getScore() );
        for (GKAbstractSafeCourse item : this.levelCourseList) {
            item.info();
        }
    }
}

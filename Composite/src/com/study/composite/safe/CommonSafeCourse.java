package com.study.composite.safe;

public class CommonSafeCourse extends GKAbstractSafeCourse {
    public CommonSafeCourse(String name, String score){
        super(name,score);
    }
    @Override
    public void info() {
        System.out.println("科目:" + this.getName());
        System.out.println("分数:" + this.getScore());
    }
}

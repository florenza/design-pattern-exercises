package com.study.main.Flyweight;
import java.util.HashMap;
import java.util.Map;

public class FlyweightFactory {
    public Map<String,ConcreateFlyweight> poll = new HashMap<>();
    public FlyweightFactory(){
        poll.put("A", new ConcreateFlyweight("A"));
        poll.put("B", new ConcreateFlyweight("B"));
        poll.put("C", new ConcreateFlyweight("C"));
    }
    public IFlyweight getFlyweight(String state){
        if (poll.containsKey(state)){
            System.out.println("享元池中有重复的key");
            return (IFlyweight) poll.get(state);
        }else {
            System.out.println("享元池中没有重复的key, 需要创建");
            poll.put(state,new ConcreateFlyweight(state));
            return (IFlyweight) poll.get(state);
        }
    }
}

package com.study.main.Flyweight;

public class ConcreateFlyweight implements IFlyweight{
    private String uniquekey;
    public ConcreateFlyweight(String key){
        this.uniquekey = key;
    }
    @Override
    public void operation(int state) {
        System.out.println("享元内部状态"+",state:"+state+",uniqueKey:"+ this.uniquekey);
    }
}

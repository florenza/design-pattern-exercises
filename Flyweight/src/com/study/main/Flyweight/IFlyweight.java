package com.study.main.Flyweight;

public interface IFlyweight {
    void operation(int state);
}

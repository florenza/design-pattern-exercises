package com.study.main.Factory;

public class Tongs implements ToolInterface{
    public String name;
    public Tongs(String name) {
        this.name = name;
    }
    @Override
    public String name() {

        return this.name;
    }

    @Override
    public void methods() {
        System.out.println(this.name);
    }
}

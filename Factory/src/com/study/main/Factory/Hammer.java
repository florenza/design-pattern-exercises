package com.study.main.Factory;

public class Hammer implements ToolInterface{
    public String name;

    public Hammer(String name) {
        this.name = name;
    }

    @Override
    public String name() {

        return this.name;
    }

    @Override
    public void methods() {
        System.out.println(this.name);
    }
}

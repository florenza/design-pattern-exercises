package com.study.main.Factory.FactoryMethod;

public class HammerFactory implements Factory{
    @Override
    public void method() {
        String hammer = new Hammer("Hammer").name();
        System.out.println(hammer);
    }
}

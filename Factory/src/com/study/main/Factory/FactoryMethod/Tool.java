package com.study.main.Factory.FactoryMethod;

public interface Tool {
    String name();
}

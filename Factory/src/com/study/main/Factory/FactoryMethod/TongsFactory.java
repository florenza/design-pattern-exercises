package com.study.main.Factory.FactoryMethod;

public class TongsFactory implements Factory{

    @Override
    public void method() {
        String tongs = new Tongs("Tongs").name();
        System.out.println(tongs);
    }
}

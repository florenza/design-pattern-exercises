package com.study.main.Factory.FactoryMethod;

public class Hammer implements Tool{
    String name;

    public Hammer(String name) {
        this.name = name;
    }

    @Override
    public String name() {
        return this.name;
    }
}

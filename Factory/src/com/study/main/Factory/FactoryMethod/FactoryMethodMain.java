package com.study.main.Factory.FactoryMethod;

public class FactoryMethodMain {
    public static void main(String[] args) {
        new HammerFactory().method();
        new TongsFactory().method();
    }
}

package com.study.main.Factory.FactoryMethod;

public class Tongs implements Tool{
    String name;

    public Tongs(String name) {
        this.name = name;
    }

    @Override
    public String name() {
        return this.name;
    }
}

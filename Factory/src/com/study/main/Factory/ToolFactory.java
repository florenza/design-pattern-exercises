package com.study.main.Factory;

public class ToolFactory {
    public ToolInterface getTool(String name){
        if(name.equals("Tongs")){
           return new Tongs(name);
        }
        if(name.equals("Hammer")){
            return new Hammer(name);
        }
        return null;
    }
}

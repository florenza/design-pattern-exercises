package com.study.main.Factory.AbstractFactory;

public class TrueColorEraser implements Eraser{
    @Override
    public void erase() {
        System.out.println("----------TrueColor Erase---------------");
    }
}

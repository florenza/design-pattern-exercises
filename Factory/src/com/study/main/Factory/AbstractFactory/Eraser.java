package com.study.main.Factory.AbstractFactory;

public interface Eraser {
    void erase();
}

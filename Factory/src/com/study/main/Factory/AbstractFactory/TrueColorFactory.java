package com.study.main.Factory.AbstractFactory;

public class TrueColorFactory implements AbstractFactory{

    @Override
    public Pencil createPencil() {
        return new TrueColorPencil();
    }

    @Override
    public Eraser createEraser() {
        return new TrueColorEraser();
    }
}

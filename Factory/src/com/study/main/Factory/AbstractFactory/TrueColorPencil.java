package com.study.main.Factory.AbstractFactory;

public class TrueColorPencil implements Pencil{
    @Override
    public void draw() {
        System.out.println("----------TrueColor Draw---------------");
    }
}

package com.study.main.Factory.AbstractFactory;

public class TestMain {
    public static void main(String[] args) {
        new ChenguangFactory().createPencil().draw();
        new TrueColorFactory().createEraser().erase();
        new ChenguangFactory().createPencil().draw();
        new TrueColorFactory().createEraser().erase();
    }
}

package com.study.main.Factory.AbstractFactory;

public interface AbstractFactory {
    Pencil createPencil();
    Eraser createEraser();
}

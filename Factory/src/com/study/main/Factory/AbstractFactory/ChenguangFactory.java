package com.study.main.Factory.AbstractFactory;

public class ChenguangFactory implements AbstractFactory{
    @Override
    public Pencil createPencil() {
        return new ChenGuangPencil();
    }

    @Override
    public Eraser createEraser() {
        return new ChenGuangEraser();
    }
}

package com.study.main.Factory.AbstractFactory;

public interface Pencil {
    void draw();
}

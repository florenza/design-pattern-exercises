package com.study.main.Factory;

public interface ToolInterface {
    public String name ();
    public void methods();
}

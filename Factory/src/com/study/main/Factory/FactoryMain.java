package com.study.main.Factory;

public class FactoryMain {
    public static void main(String[] args) {
        ToolFactory toolFactory = new ToolFactory();
        ToolInterface tongs = toolFactory.getTool("Hammer");
        tongs.methods();
    }
}

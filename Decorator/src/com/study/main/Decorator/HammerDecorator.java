package com.study.main.Decorator;

import com.study.main.Decorator.Hammer;

/**
 * 锤子装饰类
 */
public class HammerDecorator extends Hammer {
    Hammer hammer;
    public HammerDecorator(Hammer hammer) {
        super();
        this.hammer = hammer;
    }

    @Override
    String getPrice() {
        return this.hammer.getPrice();
    }

    @Override
    String getDecribe() {
        return this.hammer.getDecribe();
    }
}

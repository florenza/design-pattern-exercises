package com.study.main.Decorator;

/**
 * 锤子抽象类
 */
public abstract class Hammer {
    String name;
    String price;
    String decribe;

    public Hammer() {
    }

    abstract String getPrice();
    abstract String getDecribe();
}

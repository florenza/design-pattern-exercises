package com.study.main.Decorator;

import com.study.main.Decorator.Hammer;

public class DecoratorMain {
    public static void main(String[] args) {
        Hammer hammer = new BaseHammer("10", "锤子描述:");
        IronHammer ironHammer = new IronHammer(hammer);
        StoneHammer stoneHammer = new StoneHammer(hammer);
        System.out.println(ironHammer.getDecribe());
        System.out.println(ironHammer.getPrice());
        System.out.println(stoneHammer.getDecribe());
        System.out.println(stoneHammer.getPrice());
    }
}

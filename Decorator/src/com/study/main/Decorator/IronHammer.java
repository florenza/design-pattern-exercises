package com.study.main.Decorator;

import com.study.main.Decorator.Hammer;

/**
 * 装饰1
 */
public class IronHammer extends HammerDecorator{
    public IronHammer(Hammer hammer) {
        super(hammer);
    }

    @Override
    String getDecribe() {
        return super.getDecribe() + ",这是一个铁做的锤子";
    }

    @Override
    String getPrice() {
        return super.getPrice() + "+ 20元";
    }
}

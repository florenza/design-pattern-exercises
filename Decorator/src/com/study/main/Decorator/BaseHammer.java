package com.study.main.Decorator;

import com.study.main.Decorator.Hammer;

/**
 * 锤子实例
 */
public class BaseHammer extends Hammer {
    String price;
    String describe;
    public BaseHammer(String price,String describe) {
        super();
        this.price = price;
        this.describe = describe;
    }

    @Override
    String getPrice() {
        return this.price;
    }

    @Override
    String getDecribe() {
        return this.describe;
    }
}

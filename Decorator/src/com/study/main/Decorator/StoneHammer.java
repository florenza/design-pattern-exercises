package com.study.main.Decorator;

import com.study.main.Decorator.Hammer;

/**
 * 装饰2
 */
public class StoneHammer extends HammerDecorator{
    public StoneHammer(Hammer hammer) {
        super(hammer);
    }

    @Override
    String getPrice() {
        return super.getPrice() + "+ 10元";
    }

    @Override
    String getDecribe() {
        return super.getDecribe() + ",这是一个由石头构成的";
    }
}

package com.study.main.Prototype;

public class PrototypeMain {
    public static void main(String[] args) throws CloneNotSupportedException {
        CloneTest1 cloneSource = new CloneTest1();
        CloneTest2 cloneSource2 = new CloneTest2();
        for (int i = 0; i < 2; i++) {
            CloneTest1 cloneTest= (CloneTest1) cloneSource.clone();
            cloneTest.show();
            System.out.println(cloneTest.getName());
        }
        for (int i = 0; i < 2; i++) {
            CloneTest2 cloneTest2= (CloneTest2) cloneSource2.clone();
            cloneTest2.show();
            System.out.println(cloneTest2.getName());
        }
    }
}

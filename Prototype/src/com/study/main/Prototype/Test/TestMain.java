package com.study.main.Prototype.Test;

public class TestMain {
    public static void main(String[] args) throws CloneNotSupportedException {
        // 创建教师
        Teacher teacher = new Teacher();
        // 设置教师姓名
        teacher.setName("A");
        // 创建学生
        Student stu1 = new Student();
        // 设置学生1教师
        stu1.setTeacher(teacher);
        // 克隆学生1==>学生2
        Student stu2 = (Student) stu1.clone();
        // 设置学生2教师
        stu2.setTeacher(teacher);
        // 获取stu2老师
        Teacher stu2Teacher = stu2.getTeacher();
        // 学生2改变名字
        teacher.setName("B");
        System.out.println("学生1老师姓名:"+stu1.getTeacher().getName());
        System.out.println("学生2老师姓名:"+stu2.getTeacher().getName());
    }
}

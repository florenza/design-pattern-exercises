package com.study.main.Prototype.Test;

public class Teacher implements Cloneable {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Teacher teacher = null;

        try{
            Teacher clone = (Teacher) super.clone();
            return clone;
        }catch (CloneNotSupportedException err) {
            err.printStackTrace();
        }
        return teacher;
    }
}

package com.study.main.Prototype.Test;

public class Student implements Cloneable {
    String name;
    private Teacher teacher;

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Student student = null;
        try{
            Student clone = (Student) super.clone();
            teacher = (Teacher) this.teacher.clone();
            return clone;
        }catch (CloneNotSupportedException err) {
            err.printStackTrace();
        }
        return student;
    }
}

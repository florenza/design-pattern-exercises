package com.study.main.Prototype;

public class CloneSource implements Cloneable{

    public String getName() {
        return "克隆源";
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        CloneSource cloneSource = null;
        try{
            cloneSource = (CloneSource) super.clone();
        }
        catch (CloneNotSupportedException err){
            err.printStackTrace();
        }
        return cloneSource;
    }
}
